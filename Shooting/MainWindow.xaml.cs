﻿using System.Windows;

namespace Shooting
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            DataContext = new MainWindowViewModel();
            InitializeComponent();
        }

        public MainWindow(MainWindowViewModel dataContext)
        {
            DataContext = dataContext;
            InitializeComponent();
        }
    }
}