﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace Shooting
{
    public class MainWindowViewModel : ViewModelBase
    {
        private readonly MediaPlayer _shootSoundPlayer = new MediaPlayer();
        private readonly MediaPlayer _ammoChangeSoundPlayer = new MediaPlayer();

        private int _r1 = 100;
        private int _r2 = 200;
        private int _shotsLeft = 10;
        private int _shotsMissed;
        private int _shotsHit;
        private GeometryCollection _shotMarkers = new GeometryCollection();
        private int _shotX;
        private int _shotY;
        
        private string _shotXText;
        private bool _isFireEnabled;
        private string _shotYText;

        private int _reloadDelay;
        private Timer _reloadTimer;

        private bool _isReady;

        public bool IsReady
        {
            get => _isReady;
            set
            {
                _isReady = value;
                StatusText = ShotsLeft > 0 ? _isReady ? "Ready!" : "Reloading..." : "Out of ammo!";
                RaisePropertyChanged(nameof(IsFireEnabled));
                RaisePropertyChanged(nameof(StatusText));
            }
        }

        public string StatusText { get; private set; }

        public IShootingCoordinator Coordinator { get; set; }

        public int CanvasWidth => 500;
        public int CanvasHeight => 500;

        public int R1
        {
            get => _r1;
            set
            {
                _r1 = value;
                RaisePropertyChanged(nameof(R1));
                RaisePropertyChanged(nameof(FrontCirclePathPointCoords));
                RaisePropertyChanged(nameof(FrontCircleArcPointCoords));
                RaisePropertyChanged(nameof(FrontCircleArcSize));
            }
        }

        public int R2
        {
            get => _r2;
            set
            {
                _r2 = value;
                RaisePropertyChanged(nameof(R2));
                RaisePropertyChanged(nameof(BackCirclePathPointCoords));
                RaisePropertyChanged(nameof(BackCircleArcPointCoords));
                RaisePropertyChanged(nameof(BackCircleArcSize));
            }
        }

        public string BackCirclePathPointCoords => $"0 {R2}";

        public string BackCircleArcPointCoords => $"{R2} 0";

        public string BackCircleArcSize => $"{R2} {R2}";

        public string FrontCirclePathPointCoords => $"0 -{R1}";

        public string FrontCircleArcPointCoords => $"-{R1} 0";

        public string FrontCircleArcSize => $"{R1} {R1}";

        public int CursorX { get; set; }

        public int CursorY { get; set; }

        public int ShotX
        {
            get => _shotX;
            set
            {
                _shotX = value;
                _shotXText = $"{_shotX}";
                RaisePropertyChanged(nameof(ShotXText));
            }
        }

        public string ShotXText
        {
            get => _shotXText;
            set
            {
                _shotXText = value;
                CheckShotXText();

                RaisePropertyChanged(nameof(ShotXText));
            }
        }

        private void CheckShotXText()
        {
            var parseResult = int.TryParse(_shotXText, out int shotCoord);

            ShotX = shotCoord;
            IsFireEnabled = parseResult;
        }

        public int ShotY
        {
            get => _shotY;
            set
            {
                _shotY = value;
                _shotYText = $"{_shotY}";
                RaisePropertyChanged(nameof(ShotYText));
            }
        }

        public string ShotYText
        {
            get => _shotYText;
            set
            {
                _shotYText = value;

                CheckShotYText();

                RaisePropertyChanged(nameof(ShotYText));
            }
        }

        private void CheckShotYText()
        {
            var parseResult = int.TryParse(_shotYText, out int shotCoord);

            ShotY = shotCoord;
            IsFireEnabled = parseResult;
        }

        public int ShotsLeft
        {
            get => _shotsLeft;
            set
            {
                _shotsLeft = value;
                RaisePropertyChanged(nameof(ShotsLeft));
                CheckShotXText();
                CheckShotYText();
            }
        }

        public int ShotsMissed
        {
            get => _shotsMissed;
            set
            {
                _shotsMissed = value;
                RaisePropertyChanged(nameof(ShotsMissed));
            }
        }

        public int ShotsHit
        {
            get => _shotsHit;
            set
            {
                _shotsHit = value;
                RaisePropertyChanged(nameof(ShotsHit));
            }
        }

        public bool IsFireEnabled
        {
            get
            {
                return (_isFireEnabled && ShotX >= 0 && ShotX <= CanvasWidth
                        && ShotY >= 0 && ShotY <= CanvasHeight && ShotsLeft > 0 && IsReady);
            }
            set
            {
                _isFireEnabled = value;
                RaisePropertyChanged(nameof(IsFireEnabled));
            }
        }

        public IDictionary<string, ICommand> Commands { get; } = new Dictionary<string, ICommand>();

        public GeometryCollection ShotMarkers
        {
            get => _shotMarkers;
            set
            {
                _shotMarkers = value;
                RaisePropertyChanged(nameof(ShotMarkers));
            }
        }

        public MainWindowViewModel(int reloadDelay = 0)
        {
            Commands["CircleClickCommand"] = new RelayCommand(OnCircleClick);
            Commands["BackgroundClickCommand"] = new RelayCommand(OnBackgroundClickCommand);
            Commands["ChangeShots"] = new RelayCommand<string>(OnChangeShots);
            Commands["MouseMoveCommand"] = new RelayCommand<Point>(OnMouseMoveCommand);
            Commands["ResetCommand"] = new RelayCommand(OnResetCommand);
            Commands["FireCommand"] = new RelayCommand(OnFireCommand);
            Commands["WindowClosing"] = new RelayCommand(OnWindowClosing);

            _reloadDelay = reloadDelay;
            IsReady = true;
        }

        private void OnWindowClosing()
        {
            Coordinator?.OnShootingClosed();
        }

        public void OnFireCommand()
        {
            Shoot(CheckManualShot(), false);
        }

        private void StartReloading()
        {
            Debug.WriteLine($"Shooting: starting reload for {_reloadDelay} ms.");
            IsReady = false;
            _reloadTimer = new Timer(
                new TimerCallback(FinishReloading),
                null,
                _reloadDelay, 0);
        }

        private void FinishReloading(object state)
        {
            Debug.WriteLine("Shooting: reload finished.");
            IsReady = true;
            _reloadTimer.Dispose();
        }

        private bool CheckManualShot()
        {
            int originX = CanvasWidth / 2;
            int originY = CanvasHeight / 2;
            int x0 = ShotX - originX;
            int y0 = ShotY - originY;
            int h = x0 * x0 + y0 * y0;
            return ((h < R1 * R1 && x0 < 0 && y0 < 0) || (h > R1 * R1 && h < R2 * R2 && x0 > 0 && y0 > 0));
        }

        private void OnResetCommand()
        {
            ResetShotMarkers();
            ShotsMissed = 0;
            ShotsHit = 0;
        }

        private void OnMouseMoveCommand(Point point)
        {
            // Console.WriteLine($"{point.X} {point.Y}");
            CursorX = (int) point.X;
            CursorY = (int) point.Y;
        }

        private void OnCircleClick()
        {
            Shoot(true, true);
        }

        private void OnBackgroundClickCommand()
        {
            Shoot(false, true);
        }

        private void Shoot(bool isSuccessful, bool cursor)
        {
            if (!IsReady)
            {
                Debug.WriteLine("Shooting: reloading... skipping fire.");
                return;
            }

            StartReloading();

            if (cursor)
            {
                ShotX = CursorX;
                ShotY = CursorY;
            }

            if (ShotsLeft > 0)
            {
                if (isSuccessful)
                {
                    --ShotsLeft;
                    ++ShotsHit;

                    ShotMarkers.Add(new EllipseGeometry(new Point(ShotX, ShotY), 5, 5));

                    _shootSoundPlayer.Open(new Uri("Resources/Sounds/Hit.mp3", UriKind.Relative));
                }
                else
                {
                    _shootSoundPlayer.Open(new Uri("Resources/Sounds/Shoot.mp3", UriKind.Relative));

                    ShotMarkers.Add(new RectangleGeometry(new Rect(new Point(ShotX - 5, ShotY - 5),
                        new Point(ShotX + 5, ShotY + 5))));

                    --ShotsLeft;
                    ++ShotsMissed;
                }

                _shootSoundPlayer.Play();
            }
        }

        private void OnChangeShots(string param)
        {
            if (param == "+" && ShotsLeft < 20)
            {
                ++ShotsLeft;
                PlayAmmoChangeSound();
            }
            else if (param == "-" && ShotsLeft > 0)
            {
                --ShotsLeft;
                PlayAmmoChangeSound();
            }
        }

        private void PlayAmmoChangeSound()
        {
            _ammoChangeSoundPlayer.Open(new Uri("Resources/Sounds/AddAmmo.mp3", UriKind.Relative));
            _ammoChangeSoundPlayer.Play();
        }

        private void ResetShotMarkers()
        {
            ShotMarkers = new GeometryCollection();
        }
    }
}