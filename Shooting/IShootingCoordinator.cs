﻿namespace Shooting
{
    public interface IShootingCoordinator
    {
        void OnShootingClosed();
    }
}