﻿using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;

namespace Shooting
{
    [ValueConversion(typeof(MouseEventArgs), typeof(Point))]
    public class MouseButtonEventArgsToPointConverter : IEventArgsConverter
    {
        public object Convert(object value, object parameter)
        {
            var args = (MouseEventArgs) value;
            var element = (FrameworkElement) parameter;
            var point = args.GetPosition(element);
            return point;
        }
    }
}