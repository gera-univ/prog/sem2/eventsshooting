﻿using System;
using System.Numerics;

namespace DelannoyNumbers.Worker
{
    public class ComputationCompleteEventArgs : EventArgs
    {
        public int M;
        public int N;
        public BigInteger Result;
    }
}