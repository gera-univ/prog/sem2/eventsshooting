﻿using System;
using System.Numerics;

namespace DelannoyNumbers.Worker
{
    class DelannoyNumbers
    {
        private static BigInteger[,] _cache;

        private int _maxM;
        private int _maxN;

        public DelannoyNumbers(int maxM, int maxN)
        {
            _maxM = maxM;
            _maxN = maxN;
            _cache = new BigInteger[_maxM + 1, _maxN + 1];
        }

        public BigInteger this[int m, int n] // indexer
        {
            get
            {
                if (m > _maxM || n > _maxN)
                    throw new IndexOutOfRangeException();

                for (int i = 0; i <= m; i++)
                {
                    _cache[i, 0] = 1;
                }

                for (int i = 0; i <= n; i++)
                {
                    _cache[0, i] = 1;
                }

                for (int i = 1; i <= m; i++)
                for (int j = 1; j <= n; j++)
                    if (_cache[i, j] == 0)
                        _cache[i, j] = _cache[i - 1, j] +
                                       _cache[i - 1, j - 1] +
                                       _cache[i, j - 1];

                return _cache[m, n];
            }
        }
    }
}