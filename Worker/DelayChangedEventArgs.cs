﻿using System;

namespace DelannoyNumbers.Worker
{
    public class DelayChangedEventArgs : EventArgs
    {
        public int Delay;
    }
}