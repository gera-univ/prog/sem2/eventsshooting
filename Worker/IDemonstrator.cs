﻿using System;

namespace DelannoyNumbers.Worker
{
    public interface IDemonstrator
    {
        public event EventHandler ComputationsStarted;
        public event EventHandler ComputationsStopped;
        public event EventHandler<DelayChangedEventArgs> DelayChanged;
    }
}