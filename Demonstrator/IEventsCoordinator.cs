﻿using System.Numerics;

namespace DelannoyNumbers.Demonstrator
{
    public interface IEventsCoordinator
    {
        void OnEventsClosed();
        void OnEventsComputationComplete(in BigInteger result);
    }
}