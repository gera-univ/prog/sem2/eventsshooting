﻿using System;
using System.Diagnostics;
using System.Numerics;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using Shooting;
using DelannoyNumbers.Demonstrator;
using MainWindowViewModel = DelannoyNumbers.Demonstrator.MainWindowViewModel;

namespace Coordinator
{
    public class CoordinatorViewModel : ViewModelBase, IEventsCoordinator, IShootingCoordinator
    {
        private bool _areEventsStarted;
        private bool _isShootingStarted;

        public ICommand StartEvents { get; set; }
        public ICommand StartShooting { get; set; }

        public bool AreEventsStarted
        {
            get => _areEventsStarted;
            set
            {
                _areEventsStarted = value;
                RaisePropertyChanged(nameof(AreEventsStarted));
            }
        }

        public bool IsShootingStarted
        {
            get => _isShootingStarted;
            set
            {
                _isShootingStarted = value;
                RaisePropertyChanged(nameof(IsShootingStarted));
            }
        }

        private Window _shootingWindow;
        private Shooting.MainWindowViewModel _shootingViewModel;
        private Window _eventsWindow;
        private MainWindowViewModel _eventsViewModel;
        private string _shootingDelayLabelText;
        private int _shootingDelay;

        public string ShootingDelayLabelText
        {
            get => _shootingDelayLabelText;
            set
            {
                _shootingDelayLabelText = value;
                RaisePropertyChanged(nameof(ShootingDelayLabelText));
            }
        }

        public int ShootingDelay
        {
            get => _shootingDelay;
            set
            {
                _shootingDelay = value;
                ShootingDelayLabelText = $"Reload delay: {_shootingDelay} ms";
            }
        }

        public CoordinatorViewModel()
        {
            StartEvents = new RelayCommand(OnStartEvents);
            StartShooting = new RelayCommand(OnStartShooting);

            ShootingDelay = 0;
        }

        private void OnStartEvents()
        {
            Thread newWindowThread = new Thread(StartEventsWindow);
            newWindowThread.SetApartmentState(ApartmentState.STA);
            newWindowThread.IsBackground = true;
            newWindowThread.Start();

            AreEventsStarted = true;
        }

        private void OnStartShooting()
        {
            Thread newWindowThread = new Thread(StartShootingWindow);
            newWindowThread.SetApartmentState(ApartmentState.STA);
            newWindowThread.IsBackground = true;
            newWindowThread.Start();

            IsShootingStarted = true;
        }

        public void OnShootingClosed()
        {
            _shootingViewModel = null;
            IsShootingStarted = false;

            // Выключение Событий для нечётных вариантов.
            // Почему бы двум окнам не жить своей жизнью? Выглядит, как сужение функционала...
            CloseWindowSafe(_eventsWindow);

            Debug.WriteLine("Shooting closed");
        }

        public void OnEventsClosed()
        {
            _eventsViewModel = null;
            AreEventsStarted = false;

            Debug.WriteLine("Events closed");
        }

        public void OnEventsComputationComplete(in BigInteger result)
        {
            Debug.WriteLine($"Coordinator acquired result: {result}");

            Random _rand = new Random(result.GetHashCode()); // как-нибудь используем результат вычислений

            // окно может быть null, если выстрел произойдёт в промежутке между его созданием и созданием ViewModel
            if (_shootingViewModel != null && _shootingWindow != null)
            {
                _shootingViewModel.ShotX = _rand.Next(_shootingViewModel.CanvasWidth);
                _shootingViewModel.ShotY = _rand.Next(_shootingViewModel.CanvasHeight);
                _shootingWindow.Dispatcher.Invoke(_shootingViewModel.OnFireCommand, DispatcherPriority.Normal);
            }
            else
            {
                Debug.WriteLine("Shooting ViewModel is null, ignoring result.");
            }
        }

        private void CloseWindowSafe(Window w)
        {
            if (w == null) return;

            if (w.Dispatcher.CheckAccess())
                w.Close();
            else
                w.Dispatcher.Invoke(DispatcherPriority.Normal, new ThreadStart(w.Close));
        }

        private void StartEventsWindow()
        {
            _eventsViewModel = new DelannoyNumbers.Demonstrator.MainWindowViewModel
            {
                Coordinator = this
            };
            _eventsWindow = new DelannoyNumbers.Demonstrator.MainWindow(_eventsViewModel);
            _eventsWindow.Show();
            Dispatcher.Run();
        }

        private void StartShootingWindow()
        {
            _shootingViewModel = new Shooting.MainWindowViewModel(ShootingDelay)
            {
                Coordinator = this
            };
            _shootingWindow = new Shooting.MainWindow(_shootingViewModel);
            _shootingWindow.Show();
            Dispatcher.Run();
        }
    }
}