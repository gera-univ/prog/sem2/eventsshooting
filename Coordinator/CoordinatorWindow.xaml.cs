﻿using System.Windows;

namespace Coordinator
{
    public partial class CoordinatorWindow : Window
    {
        public CoordinatorWindow()
        {
            InitializeComponent();
            var viewModel = new CoordinatorViewModel();
            DataContext = viewModel;
        }
    }
}
